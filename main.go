package main

import (
	"gitee.com/dabolau/lakaiService/database"
	"gitee.com/dabolau/lakaiService/model"
	"gitee.com/dabolau/lakaiService/router"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

// 入口程序
// https://gin-gonic.com/
// https://gorm.io/
func main() {
	// 数据库连接
	database.OpenSQLite3()
	// 数据库迁移
	database.DB.AutoMigrate(&model.Video{}, &model.URL{})
	database.DB.AutoMigrate(&model.Feedback{}, &model.Version{}, &model.User{})
	// 定义默认路由
	r := gin.Default()
	// 跨域
	r.Use(cors.Default())
	// 加载路由
	router.Home(r)
	router.Account(r)
	router.User(r)
	router.Feedback(r)
	router.Version(r)
	router.URL(r)
	router.Video(r)
	// 启动服务
	r.Run("0.0.0.0:8080") // 监听服务 0.0.0.0:8080
}
