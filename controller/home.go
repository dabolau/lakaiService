package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// 响应数据
type ResponseHome struct {
	Message    string
	StatusCode uint
}

// NotFoundHandler 网页未找到
// https://gin-gonic.com/docs/examples/rendering/
func NotFoundHandler(c *gin.Context) {
	c.JSON(http.StatusOK, ResponseHome{
		Message:    "网页未找到",
		StatusCode: http.StatusNotFound,
	})
}

// HomeHandler 首页信息
// https://gin-gonic.com/docs/examples/rendering/
func HomeHandler(c *gin.Context) {
	c.JSON(http.StatusOK, ResponseHome{
		Message:    "拉开接口1.0",
		StatusCode: http.StatusOK,
	})
}
