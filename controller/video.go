package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"gitee.com/dabolau/lakaiService/database"
	"gitee.com/dabolau/lakaiService/model"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// 响应数据
type ResponseVideo struct {
	Message    string
	StatusCode uint
}

// 响应多条数据
type ResponseMultipleVideo struct {
	Datas      []model.Video
	Message    string
	StatusCode uint
}

// 响应单条数据
type ResponseSingleVideo struct {
	Data       model.Video
	Message    string
	StatusCode uint
}

// VideoHandler 视频信息
// https://gorm.io/zh_CN/docs/query.html
func VideoHandler(c *gin.Context) {
	var obj *gorm.DB
	var videos []model.Video
	var videoName string = ""
	var videoCategory string = ""
	var videoType string = ""
	var videoArea string = ""
	var videoYear string = ""
	var page int = 1
	var pageSize int = 10
	var totalSize int64 = 0
	// 获取参数
	videoName = c.Query("text")
	videoType = c.Query("type")
	videoCategory = c.Query("category")
	videoArea = c.Query("area")
	videoYear = c.Query("year")
	if !(c.Query("size") == "") {
		pageSize, _ = strconv.Atoi(c.Query("size"))
	}
	if !(c.Query("page") == "") {
		page, _ = strconv.Atoi(c.Query("page"))
	}
	// 查询数据
	obj = database.DB.Model(&videos)
	obj.Where("name LIKE ?", fmt.Sprintf("%%%v%%", videoName))
	obj.Where("category LIKE ?", fmt.Sprintf("%%%v%%", videoCategory))
	obj.Where("type LIKE ?", fmt.Sprintf("%%%v%%", videoType))
	obj.Where("area LIKE ?", fmt.Sprintf("%%%v%%", videoArea))
	obj.Where("year LIKE ?", fmt.Sprintf("%%%v%%", videoYear))
	obj.Order("ID DESC")
	// 数据总量
	obj.Count(&totalSize)
	// 分页查询
	obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&videos)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseMultipleVideo{
			Datas:      videos,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseVideo{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// VideoDetailHandler 视频详情
// https://gorm.io/zh_CN/docs/query.html
func VideoDetailHandler(c *gin.Context) {
	var obj *gorm.DB
	var video model.Video
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseVideo{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&video, id)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleVideo{
			Data:       video,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseVideo{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// VideoAddHandler 视频添加
// https://gorm.io/zh_CN/docs/create.html
func VideoAddHandler(c *gin.Context) {
	var obj *gorm.DB
	var requestVideo model.Video
	// 绑定前端数据
	c.Bind(&requestVideo)
	// 参数错误
	if requestVideo.Name == "" {
		c.JSON(http.StatusForbidden, ResponseVideo{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 添加数据
	obj = database.DB.Create(&requestVideo)
	// 添加成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleVideo{
			Data:       requestVideo,
			Message:    "添加成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 添加失败
	c.JSON(http.StatusOK, ResponseVideo{
		Message:    "添加失败",
		StatusCode: http.StatusForbidden,
	})
}

// VideoChangeHandler 视频编辑
// https://gorm.io/zh_CN/docs/update.html
func VideoChangeHandler(c *gin.Context) {
	var obj *gorm.DB
	var video model.Video
	var requestVideo model.Video
	// 获取参数
	var id string = c.Query("id")
	// 绑定前端数据
	c.Bind(&requestVideo)
	// 参数错误
	if id == "" || requestVideo.Name == "" {
		c.JSON(http.StatusForbidden, ResponseVideo{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&video, id)
	// 编辑成功
	if obj.RowsAffected > 0 {
		video.Name = requestVideo.Name
		video.Type = requestVideo.Type
		video.Area = requestVideo.Area
		video.Year = requestVideo.Year
		video.Actor = requestVideo.Actor
		video.Director = requestVideo.Director
		video.Status = requestVideo.Status
		video.Description = requestVideo.Description
		video.URL = requestVideo.URL
		// 保存数据
		database.DB.Save(&video)
		c.JSON(http.StatusOK, ResponseSingleVideo{
			Data:       video,
			Message:    "编辑成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 编辑失败
	c.JSON(http.StatusOK, ResponseVideo{
		Message:    "编辑失败",
		StatusCode: http.StatusForbidden,
	})
}

// VideoDeleteHandler 视频删除
// https://gorm.io/zh_CN/docs/delete.html
func VideoDeleteHandler(c *gin.Context) {
	var obj *gorm.DB
	var video model.Video
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseVideo{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 数据查询
	obj = database.DB.First(&video, id)
	// 删除成功
	if obj.RowsAffected > 0 {
		database.DB.Delete(&video, id)
		c.JSON(http.StatusOK, ResponseSingleVideo{
			Data:       video,
			Message:    "删除成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 删除失败
	c.JSON(http.StatusOK, ResponseVideo{
		Message:    "删除失败",
		StatusCode: http.StatusForbidden,
	})
}
