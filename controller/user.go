package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"gitee.com/dabolau/lakaiService/database"
	"gitee.com/dabolau/lakaiService/model"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// 响应数据
type ResponseUser struct {
	Message    string
	StatusCode uint
}

// 响应多条数据
type ResponseMultipleUser struct {
	Datas      []model.User
	Message    string
	StatusCode uint
}

// 响应单条数据
type ResponseSingleUser struct {
	Data       model.User
	Message    string
	StatusCode uint
}

// UserHandler 用户信息
// https://gorm.io/zh_CN/docs/query.html
func UserHandler(c *gin.Context) {
	var obj *gorm.DB
	var users []model.User
	var userUsername string = ""
	var page int = 1
	var pageSize int = 10
	var totalSize int64 = 0
	// 获取参数
	userUsername = c.Query("text")
	if !(c.Query("size") == "") {
		pageSize, _ = strconv.Atoi(c.Query("size"))
	}
	if !(c.Query("page") == "") {
		page, _ = strconv.Atoi(c.Query("page"))
	}
	// 查询数据
	obj = database.DB.Model(&users)
	obj.Where("username LIKE ?", fmt.Sprintf("%%%v%%", userUsername))
	obj.Order("ID DESC")
	// 数据总量
	obj.Count(&totalSize)
	// 分页查询
	obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&users)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseMultipleUser{
			Datas:      users,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseUser{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// UserDetailHandler 用户详情
// https://gorm.io/zh_CN/docs/query.html
func UserDetailHandler(c *gin.Context) {
	var obj *gorm.DB
	var user model.User
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseUser{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&user, id)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleUser{
			Data:       user,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseUser{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// UserAddHandler 用户添加
// https://gorm.io/zh_CN/docs/create.html
func UserAddHandler(c *gin.Context) {
	var obj *gorm.DB
	var requestUser model.User
	// 绑定前端数据
	c.Bind(&requestUser)
	// 参数错误
	if requestUser.Username == "" {
		c.JSON(http.StatusForbidden, ResponseUser{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 添加数据
	obj = database.DB.Create(&requestUser)
	// 添加成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleUser{
			Data:       requestUser,
			Message:    "添加成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 添加失败
	c.JSON(http.StatusOK, ResponseUser{
		Message:    "添加失败",
		StatusCode: http.StatusForbidden,
	})
}

// UserChangeHandler 用户编辑
// https://gorm.io/zh_CN/docs/update.html
func UserChangeHandler(c *gin.Context) {
	var obj *gorm.DB
	var user model.User
	var requestUser model.User
	// 获取参数
	var id string = c.Query("id")
	// 绑定前端数据
	c.Bind(&requestUser)
	// 参数错误
	if id == "" || requestUser.Username == "" {
		c.JSON(http.StatusForbidden, ResponseUser{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&user, id)
	// 编辑成功
	if obj.RowsAffected > 0 {
		user.Username = requestUser.Username
		user.Email = requestUser.Email
		user.Phone = requestUser.Phone
		user.Password = requestUser.Password
		user.Nickname = requestUser.Nickname
		user.Description = requestUser.Description
		// 保存数据
		database.DB.Save(&user)
		c.JSON(http.StatusOK, ResponseSingleUser{
			Data:       user,
			Message:    "编辑成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 编辑失败
	c.JSON(http.StatusOK, ResponseUser{
		Message:    "编辑失败",
		StatusCode: http.StatusForbidden,
	})
}

// UserDeleteHandler 用户删除
// https://gorm.io/zh_CN/docs/delete.html
func UserDeleteHandler(c *gin.Context) {
	var obj *gorm.DB
	var user model.User
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseUser{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 数据查询
	obj = database.DB.First(&user, id)
	// 删除成功
	if obj.RowsAffected > 0 {
		database.DB.Delete(&user, id)
		c.JSON(http.StatusOK, ResponseSingleUser{
			Data:       user,
			Message:    "删除成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 删除失败
	c.JSON(http.StatusOK, ResponseUser{
		Message:    "删除失败",
		StatusCode: http.StatusForbidden,
	})
}
