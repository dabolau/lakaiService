package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"gitee.com/dabolau/lakaiService/database"
	"gitee.com/dabolau/lakaiService/model"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// 响应数据
type ResponseVersion struct {
	Message    string
	StatusCode uint
}

// 响应多条数据
type ResponseMultipleVersion struct {
	Datas      []model.Version
	Message    string
	StatusCode uint
}

// 响应单条数据
type ResponseSingleVersion struct {
	Data       model.Version
	Message    string
	StatusCode uint
}

// VersionHandler 版本信息
// https://gorm.io/zh_CN/docs/query.html
func VersionHandler(c *gin.Context) {
	var obj *gorm.DB
	var versions []model.Version
	var versionName string = ""
	var page int = 1
	var pageSize int = 10
	var totalSize int64 = 0
	// 获取参数
	versionName = c.Query("text")
	if !(c.Query("size") == "") {
		pageSize, _ = strconv.Atoi(c.Query("size"))
	}
	if !(c.Query("page") == "") {
		page, _ = strconv.Atoi(c.Query("page"))
	}
	// 查询数据
	obj = database.DB.Model(&versions)
	obj.Where("name LIKE ?", fmt.Sprintf("%%%v%%", versionName))
	obj.Order("ID DESC")
	// 数据总量
	obj.Count(&totalSize)
	// 分页查询
	obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&versions)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseMultipleVersion{
			Datas:      versions,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseVersion{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// VersionDetailHandler 版本详情
// https://gorm.io/zh_CN/docs/query.html
func VersionDetailHandler(c *gin.Context) {
	var obj *gorm.DB
	var version model.Version
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseVersion{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&version, id)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleVersion{
			Data:       version,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseVersion{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// VersionAddHandler 版本添加
// https://gorm.io/zh_CN/docs/create.html
func VersionAddHandler(c *gin.Context) {
	var obj *gorm.DB
	var requestVersion model.Version
	// 绑定前端数据
	c.Bind(&requestVersion)
	// 参数错误
	if requestVersion.Name == "" {
		c.JSON(http.StatusForbidden, ResponseVersion{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 添加数据
	obj = database.DB.Create(&requestVersion)
	// 添加成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleVersion{
			Data:       requestVersion,
			Message:    "添加成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 添加失败
	c.JSON(http.StatusOK, ResponseVersion{
		Message:    "添加失败",
		StatusCode: http.StatusForbidden,
	})
}

// VersionChangeHandler 版本编辑
// https://gorm.io/zh_CN/docs/update.html
func VersionChangeHandler(c *gin.Context) {
	var obj *gorm.DB
	var version model.Version
	var requestVersion model.Version
	// 获取参数
	var id string = c.Query("id")
	// 绑定前端数据
	c.Bind(&requestVersion)
	// 参数错误
	if id == "" || requestVersion.Name == "" {
		c.JSON(http.StatusForbidden, ResponseVersion{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&version, id)
	// 编辑成功
	if obj.RowsAffected > 0 {
		version.Name = requestVersion.Name
		version.Version = requestVersion.Version
		version.URL = requestVersion.URL
		version.Description = requestVersion.Description
		// 保存数据
		database.DB.Save(&version)
		c.JSON(http.StatusOK, ResponseSingleVersion{
			Data:       version,
			Message:    "编辑成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 编辑失败
	c.JSON(http.StatusOK, ResponseVersion{
		Message:    "编辑失败",
		StatusCode: http.StatusForbidden,
	})
}

// VersionDeleteHandler 版本删除
// https://gorm.io/zh_CN/docs/delete.html
func VersionDeleteHandler(c *gin.Context) {
	var obj *gorm.DB
	var version model.Version
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseVersion{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 数据查询
	obj = database.DB.First(&version, id)
	// 删除成功
	if obj.RowsAffected > 0 {
		database.DB.Delete(&version, id)
		c.JSON(http.StatusOK, ResponseSingleVersion{
			Data:       version,
			Message:    "删除成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 删除失败
	c.JSON(http.StatusOK, ResponseVersion{
		Message:    "删除失败",
		StatusCode: http.StatusForbidden,
	})
}
