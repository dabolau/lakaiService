package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"gitee.com/dabolau/lakaiService/database"
	"gitee.com/dabolau/lakaiService/model"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// 响应数据
type ResponseURL struct {
	Message    string
	StatusCode uint
}

// 响应多条数据
type ResponseMultipleURL struct {
	Datas      []model.URL
	Message    string
	StatusCode uint
}

// 响应单条数据
type ResponseSingleURL struct {
	Data       model.URL
	Message    string
	StatusCode uint
}

// URLHandler 资源信息
// https://gorm.io/zh_CN/docs/query.html
func URLHandler(c *gin.Context) {
	var obj *gorm.DB
	var urls []model.URL
	var urlName string = ""
	var urlVideo string = ""
	var page int = 1
	var pageSize int = 10
	var totalSize int64 = 0
	// 获取参数
	urlName = c.Query("text")
	urlVideo = c.Query("video")
	if !(c.Query("size") == "") {
		pageSize, _ = strconv.Atoi(c.Query("size"))
	}
	if !(c.Query("page") == "") {
		page, _ = strconv.Atoi(c.Query("page"))
	}
	// 查询数据
	obj = database.DB.Model(&urls)
	obj.Where("name LIKE ?", fmt.Sprintf("%%%v%%", urlName))
	obj.Where("video LIKE ?", fmt.Sprintf("%%%v%%", urlVideo))
	obj.Order("ID DESC")
	// 数据总量
	obj.Count(&totalSize)
	// 分页查询
	obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&urls)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseMultipleURL{
			Datas:      urls,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseURL{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// URLDetailHandler 资源详情
// https://gorm.io/zh_CN/docs/query.html
func URLDetailHandler(c *gin.Context) {
	var obj *gorm.DB
	var url model.URL
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseURL{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&url, id)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleURL{
			Data:       url,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseURL{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// URLAddHandler 资源添加
// https://gorm.io/zh_CN/docs/create.html
func URLAddHandler(c *gin.Context) {
	var obj *gorm.DB
	var requestURL model.URL
	// 绑定前端数据
	c.Bind(&requestURL)
	// 参数错误
	if requestURL.Name == "" {
		c.JSON(http.StatusForbidden, ResponseURL{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 添加数据
	obj = database.DB.Create(&requestURL)
	// 添加成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleURL{
			Data:       requestURL,
			Message:    "添加成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 添加失败
	c.JSON(http.StatusOK, ResponseURL{
		Message:    "添加失败",
		StatusCode: http.StatusForbidden,
	})
}

// URLChangeHandler 资源编辑
// https://gorm.io/zh_CN/docs/update.html
func URLChangeHandler(c *gin.Context) {
	var obj *gorm.DB
	var url model.URL
	var requestURL model.URL
	// 获取参数
	var id string = c.Query("id")
	// 绑定前端数据
	c.Bind(&requestURL)
	// 参数错误
	if id == "" || requestURL.Name == "" {
		c.JSON(http.StatusForbidden, ResponseURL{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&url, id)
	// 编辑成功
	if obj.RowsAffected > 0 {
		url.Name = requestURL.Name
		url.Video = requestURL.Video
		url.Tag = requestURL.Tag
		url.URL = requestURL.URL
		// 保存数据
		database.DB.Save(&url)
		c.JSON(http.StatusOK, ResponseSingleURL{
			Data:       url,
			Message:    "编辑成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 编辑失败
	c.JSON(http.StatusOK, ResponseURL{
		Message:    "编辑失败",
		StatusCode: http.StatusForbidden,
	})
}

// URLDeleteHandler 资源删除
// https://gorm.io/zh_CN/docs/delete.html
func URLDeleteHandler(c *gin.Context) {
	var obj *gorm.DB
	var url model.URL
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseURL{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 数据查询
	obj = database.DB.First(&url, id)
	// 删除成功
	if obj.RowsAffected > 0 {
		database.DB.Delete(&url, id)
		c.JSON(http.StatusOK, ResponseSingleURL{
			Data:       url,
			Message:    "删除成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 删除失败
	c.JSON(http.StatusOK, ResponseURL{
		Message:    "删除失败",
		StatusCode: http.StatusForbidden,
	})
}
