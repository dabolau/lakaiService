package controller

import (
	"fmt"
	"net/http"
	"strconv"

	"gitee.com/dabolau/lakaiService/database"
	"gitee.com/dabolau/lakaiService/model"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// 响应数据
type ResponseFeedback struct {
	Message    string
	StatusCode uint
}

// 响应多条数据
type ResponseMultipleFeedback struct {
	Datas      []model.Feedback
	Message    string
	StatusCode uint
}

// 响应单条数据
type ResponseSingleFeedback struct {
	Data       model.Feedback
	Message    string
	StatusCode uint
}

// FeedbackHandler 资源信息
// https://gorm.io/zh_CN/docs/query.html
func FeedbackHandler(c *gin.Context) {
	var obj *gorm.DB
	var feedbacks []model.Feedback
	var feedbackName string = ""
	var page int = 1
	var pageSize int = 10
	var totalSize int64 = 0
	// 获取参数
	feedbackName = c.Query("text")
	if !(c.Query("size") == "") {
		pageSize, _ = strconv.Atoi(c.Query("size"))
	}
	if !(c.Query("page") == "") {
		page, _ = strconv.Atoi(c.Query("page"))
	}
	// 查询数据
	obj = database.DB.Model(&feedbacks)
	obj.Where("name LIKE ?", fmt.Sprintf("%%%v%%", feedbackName))
	obj.Order("ID DESC")
	// 数据总量
	obj.Count(&totalSize)
	// 分页查询
	obj.Offset((page - 1) * pageSize).Limit(pageSize).Find(&feedbacks)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseMultipleFeedback{
			Datas:      feedbacks,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseFeedback{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// FeedbackDetailHandler 资源详情
// https://gorm.io/zh_CN/docs/query.html
func FeedbackDetailHandler(c *gin.Context) {
	var obj *gorm.DB
	var feedback model.Feedback
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseFeedback{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&feedback, id)
	// 查询成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleFeedback{
			Data:       feedback,
			Message:    "查询成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 查询失败
	c.JSON(http.StatusOK, ResponseFeedback{
		Message:    "查询失败",
		StatusCode: http.StatusNotFound,
	})
}

// FeedbackAddHandler 资源添加
// https://gorm.io/zh_CN/docs/create.html
func FeedbackAddHandler(c *gin.Context) {
	var obj *gorm.DB
	var requestFeedback model.Feedback
	// 绑定前端数据
	c.Bind(&requestFeedback)
	// 参数错误
	if requestFeedback.Name == "" {
		c.JSON(http.StatusForbidden, ResponseFeedback{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 添加数据
	obj = database.DB.Create(&requestFeedback)
	// 添加成功
	if obj.RowsAffected > 0 {
		c.JSON(http.StatusOK, ResponseSingleFeedback{
			Data:       requestFeedback,
			Message:    "添加成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 添加失败
	c.JSON(http.StatusOK, ResponseFeedback{
		Message:    "添加失败",
		StatusCode: http.StatusForbidden,
	})
}

// FeedbackChangeHandler 资源编辑
// https://gorm.io/zh_CN/docs/update.html
func FeedbackChangeHandler(c *gin.Context) {
	var obj *gorm.DB
	var feedback model.Feedback
	var requestFeedback model.Feedback
	// 获取参数
	var id string = c.Query("id")
	// 绑定前端数据
	c.Bind(&requestFeedback)
	// 参数错误
	if id == "" || requestFeedback.Name == "" {
		c.JSON(http.StatusForbidden, ResponseFeedback{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.First(&feedback, id)
	// 编辑成功
	if obj.RowsAffected > 0 {
		feedback.Name = requestFeedback.Name
		feedback.Description = requestFeedback.Description
		feedback.Email = requestFeedback.Email
		// 保存数据
		database.DB.Save(&feedback)
		c.JSON(http.StatusOK, ResponseSingleFeedback{
			Data:       feedback,
			Message:    "编辑成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 编辑失败
	c.JSON(http.StatusForbidden, ResponseFeedback{
		Message:    "编辑失败",
		StatusCode: http.StatusForbidden,
	})
}

// FeedbackDeleteHandler 资源删除
// https://gorm.io/zh_CN/docs/delete.html
func FeedbackDeleteHandler(c *gin.Context) {
	var obj *gorm.DB
	var feedback model.Feedback
	// 获取参数
	var id string = c.Query("id")
	// 参数错误
	if id == "" {
		c.JSON(http.StatusForbidden, ResponseFeedback{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 数据查询
	obj = database.DB.First(&feedback, id)
	// 删除成功
	if obj.RowsAffected > 0 {
		database.DB.Delete(&feedback, id)
		c.JSON(http.StatusOK, ResponseSingleFeedback{
			Data:       feedback,
			Message:    "删除成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 删除失败
	c.JSON(http.StatusOK, ResponseFeedback{
		Message:    "删除失败",
		StatusCode: http.StatusForbidden,
	})
}
