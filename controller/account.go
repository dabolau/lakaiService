package controller

import (
	"fmt"
	"net/http"

	"gitee.com/dabolau/lakaiService/common"
	"gitee.com/dabolau/lakaiService/database"
	"gitee.com/dabolau/lakaiService/model"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// 请求数据
type RequestAccount struct {
	model.User
	NewPassword     string `form:"newpassword" json:"NewPassword"`
	ConfirmPassword string `form:"confirmpassword" json:"ConfirmPassword"`
}

// 响应数据
type ResponseAccount struct {
	Message    string
	StatusCode uint
}

// 响应编号和令牌数据
type ResponseIDAndBearerToken struct {
	BearerToken string
	ID          uint
	Message     string
	StatusCode  uint
}

// 响应编号数据
type ResponseID struct {
	ID         uint
	Message    string
	StatusCode uint
}

// 响应错误数据
type ResponseError struct {
	Error      error
	Message    string
	StatusCode uint
}

// AccountHandler 帐号信息
// https://gorm.io/zh_CN/docs/query.html
func AccountHandler(c *gin.Context) {
	// 查询成功
	c.JSON(http.StatusOK, ResponseAccount{
		Message:    "查询成功",
		StatusCode: http.StatusOK,
	})
}

// LoginHandler 帐号登录
// https://gorm.io/zh_CN/docs/query.html
func LoginHandler(c *gin.Context) {
	var obj *gorm.DB
	var account model.User
	var requestAccount model.User
	// 绑定前端数据
	c.Bind(&requestAccount)
	// 参数错误
	if requestAccount.Username == "" || requestAccount.Password == "" {
		c.JSON(http.StatusForbidden, ResponseAccount{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.Where(&model.User{
		Username: requestAccount.Username,
		Password: requestAccount.Password,
	}).Or(&model.User{
		Email:    requestAccount.Username,
		Password: requestAccount.Password,
	}).Or(&model.User{
		Phone:    requestAccount.Username,
		Password: requestAccount.Password,
	}).First(&account)
	// 登录成功
	if obj.RowsAffected > 0 {
		// 获取令牌
		token, _ := common.GenerateToken(account.Username)
		c.JSON(http.StatusOK, ResponseIDAndBearerToken{
			BearerToken: token,
			ID:          account.ID,
			Message:     "登录成功",
			StatusCode:  http.StatusOK,
		})
		return
	}
	// 登录失败
	c.JSON(http.StatusOK, ResponseAccount{
		Message:    "登录失败",
		StatusCode: http.StatusNotFound,
	})
}

// RegisterHandler 帐号注册
// https://gorm.io/zh_CN/docs/query.html
func RegisterHandler(c *gin.Context) {
	var obj *gorm.DB
	var account model.User
	var requestAccount model.User
	// 绑定前端数据
	c.Bind(&requestAccount)
	// 参数错误
	if requestAccount.Username == "" || requestAccount.Email == "" || requestAccount.Phone == "" || requestAccount.Password == "" {
		c.JSON(http.StatusForbidden, ResponseAccount{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.Where(&model.User{
		Username: requestAccount.Username,
	}).Or(&model.User{
		Email: requestAccount.Email,
	}).Or(&model.User{
		Phone: requestAccount.Phone,
	}).First(&account)
	// 注册成功
	if obj.RowsAffected < 1 {
		// 添加数据
		database.DB.Create(&requestAccount)
		c.JSON(http.StatusOK, ResponseID{
			ID:         requestAccount.ID,
			Message:    "注册成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 注册失败
	c.JSON(http.StatusOK, ResponseAccount{
		Message:    "注册失败",
		StatusCode: http.StatusNotFound,
	})
}

// ChangePasswordHandler 修改密码
// https://gorm.io/zh_CN/docs/query.html
func ChangePasswordHandler(c *gin.Context) {
	var obj *gorm.DB
	var account model.User
	var requestAccount RequestAccount
	// 绑定前端数据
	c.Bind(&requestAccount)
	// 参数错误
	if requestAccount.Username == "" || requestAccount.Password == "" || requestAccount.NewPassword == "" || requestAccount.ConfirmPassword == "" {
		c.JSON(http.StatusForbidden, ResponseAccount{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 两次密码验证
	if requestAccount.NewPassword != requestAccount.ConfirmPassword {
		c.JSON(http.StatusOK, ResponseAccount{
			Message:    "两次密码不相同",
			StatusCode: http.StatusNotFound,
		})
		return
	}
	// 查询数据
	obj = database.DB.Where(&model.User{
		Username: requestAccount.Username,
		Password: requestAccount.Password,
	}).Or(&model.User{
		Email:    requestAccount.Username,
		Password: requestAccount.Password,
	}).Or(&model.User{
		Phone:    requestAccount.Username,
		Password: requestAccount.Password,
	}).First(&account)
	// 修改成功
	if obj.RowsAffected > 0 {
		// 更新数据
		account.Password = requestAccount.NewPassword
		database.DB.Save(&account)
		c.JSON(http.StatusOK, ResponseID{
			ID:         account.ID,
			Message:    "修改成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 修改失败
	c.JSON(http.StatusOK, ResponseAccount{
		Message:    "修改失败",
		StatusCode: http.StatusNotFound,
	})
}

// GetPasswordHandler 找回密码
// https://gorm.io/zh_CN/docs/query.html
func GetPasswordHandler(c *gin.Context) {
	var obj *gorm.DB
	var account model.User
	var requestAccount model.User
	// 绑定前端数据
	c.Bind(&requestAccount)
	// 参数错误
	if requestAccount.Email == "" {
		c.JSON(http.StatusForbidden, ResponseAccount{
			Message:    "参数错误",
			StatusCode: http.StatusForbidden,
		})
		return
	}
	// 查询数据
	obj = database.DB.Where(&model.User{
		Email: requestAccount.Email,
	}).First(&account)
	// 找回成功
	if obj.RowsAffected > 0 {
		// 发送文本邮件
		// err := common.SendTextEMail(account.Email, "找到您的信息", fmt.Sprintf("您的账号：[%v]，密码：[%v]，请及时修改。", account.Username, account.Password))
		// 发送超文本标记邮件
		err := common.SendHTMLEMail(account.Email, "找到您的信息", fmt.Sprintf(common.HTMLEMailTemplate, account.Username, account.Password))
		if err != nil {
			c.JSON(http.StatusOK, ResponseError{
				Error:      err,
				Message:    "发送失败",
				StatusCode: http.StatusNotFound,
			})
			return
		}
		c.JSON(http.StatusOK, ResponseID{
			ID:         account.ID,
			Message:    "找回成功",
			StatusCode: http.StatusOK,
		})
		return
	}
	// 找回失败
	c.JSON(http.StatusOK, ResponseAccount{
		Message:    "找回失败",
		StatusCode: http.StatusNotFound,
	})
}

// LogoutHandler 帐号注销
// https://gorm.io/zh_CN/docs/query.html
func LogoutHandler(c *gin.Context) {
	// 注销成功
	c.JSON(http.StatusOK, ResponseAccount{
		Message:    "注销成功",
		StatusCode: http.StatusOK,
	})
}
