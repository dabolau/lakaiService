# 拉开接口

拉开接口是款影视后端程序，为影视点播提供服务。

### 首页信息

| 地址   | 方法    | 参数  |
| ---- | ----- | --- |
| /    | `GET` |     |
| /404 | `GET` |     |

### 帐号信息

| 地址                      | 方法     | 参数                                                    |
| ----------------------- | ------ | ----------------------------------------------------- |
| /account/               | `GET`  |                                                       |
| /account/login          | `POST` | `Username` `Password`                                 |
| /account/register       | `POST` | `Username` `Email` `Phone` `Password`                 |
| /account/changepassword | `POST` | `Username` `Password` `NewPassword` `ConfirmPassword` |
| /account/getpassword    | `POST` | `Email`                                               |
| /account/logout         | `POST` |                                                       |

### 用户信息

| 地址                         | 方法     | 参数                                                                  |
| -------------------------- | ------ | ------------------------------------------------------------------- |
| /user?text=&size=10&page=1 | `GET`  | `text` `size` `page`                                                |
| /user/detail?id=1          | `GET`  | `id`                                                                |
| /user/add                  | `POST` | `Username` `Email` `Phone` `Password` `Nickname` `Description`      |
| /user/change?id=1          | `POST` | `id` `Username` `Email` `Phone` `Password` `Nickname` `Description` |
| /user/delete?id=1          | `POST` | `id`                                                                |

用户信息需要验证令牌后才能操作

### 视频信息

| 地址                                                       | 方法     | 参数                                                                               |
| -------------------------------------------------------- | ------ | -------------------------------------------------------------------------------- |
| /video/?text=&category=&type=&area=&year=&size=10&page=1 | `GET`  | `text` `category` `type` `area` `year` `size` `page`                             |
| /video/detail?id=1                                       | `GET`  | `id`                                                                             |
| /video/add                                               | `POST` | `Name` `Type` `Area` `Year` `Actor` `Director` `Status` `Description` `URL`      |
| /video/change?id=1                                       | `POST` | `id` `Name` `Type` `Area` `Year` `Actor` `Director` `Status` `Description` `URL` |
| /video/delete?id=1                                       | `POST` | `id`                                                                             |

### 资源信息

| 地址                                | 方法     | 参数                              |
| --------------------------------- | ------ | ------------------------------- |
| /url/?text=&video=&size=10&page=1 | `GET`  | `text` `video` `size` `page`    |
| /url/detail?id=1                  | `GET`  | `id`                            |
| /url/add                          | `POST` | `Name` `Video` `Tag` `URL`      |
| /url/change?id=1                  | `POST` | `id` `Name` `Video` `Tag` `URL` |
| /url/delete?id=1                  | `POST` | `id`                            |

### 意见信息

| 地址                             | 方法     | 参数                                |
| ------------------------------ | ------ | --------------------------------- |
| /feedback?text=&size=10&page=1 | `GET`  | `text` `size` `page`              |
| /feedback/detail?id=1          | `GET`  | `id`                              |
| /feedback/add                  | `POST` | `Name` `Description` `Email`      |
| /feedback/change?id=1          | `POST` | `id` `Name` `Description` `Email` |
| /feedback/delete?id=1          | `POST` | `id`                              |

### 版本信息

| 地址                            | 方法     | 参数                                        |
| ----------------------------- | ------ | ----------------------------------------- |
| /version?text=&size=10&page=1 | `GET`  | `text` `size` `page`                      |
| /version/detail?id=1          | `GET`  | `id`                                      |
| /version/add                  | `POST` | `Name` `Version` `Description` `URL`      |
| /version/change?id=1          | `POST` | `id` `Name` `Version` `Description` `URL` |
| /version/delete?id=1          | `POST` | `id`                                      |

### 运行程序

* 构建可执行文件

```bash
go build -o lakaiService
```

* 运行可执行文件

```bash
./lakaiService
```

### 运行程序（dockerfile）

> 在执行下面操作时需要先安装好 docker

* 构建镜像

```bash
docker build -t lakai-service:latest .
```

* 运行容器

```bash
docker run -d -p 8080:8080 --name lakai-service lakai-service:latest
```

### 运行程序（docker-compose）

> 在执行下面操作时需要先安装好 docker-compose

* 运行容器

```bash
docker-compose up -d
```