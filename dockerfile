# 基础镜像
FROM ubuntu:20.04

# 复制文件到容器中
ADD . /lakaiService

# 设置工作目录
WORKDIR /lakaiService

# 执行容器启动命令
CMD ["./lakaiService"]
