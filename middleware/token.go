package middleware

import (
	"log"
	"net/http"
	"strings"

	"gitee.com/dabolau/lakaiService/common"
	"github.com/gin-gonic/gin"
)

// AuthToken 用户验证中间件（Token）
// https://gin-gonic.com/docs/examples/custom-middleware/
// https://github.com/dgrijalva/jwt-go
// 登录成功 生成令牌
// token, err := common.GenerateToken("username")
// 用户授权 解析令牌
// claims, err := common.ParseToken("token")
func AuthToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		// 通过请求头获取Token
		authHeader := c.Request.Header.Get("Authorization")
		// 参数错误导致的授权失败
		if authHeader == "" {
			c.JSON(http.StatusForbidden, gin.H{
				"StatusCode": http.StatusForbidden,
				"Message":    "授权失败",
			})
			c.Abort()
			return
		}
		// 按空格分割请求头中的参数
		parts := strings.Split(authHeader, " ")
		// 格式错误导致的授权失败
		if !(parts[0] == "Bearer" && len(parts) == 2) {
			c.JSON(http.StatusForbidden, gin.H{
				"StatusCode": http.StatusForbidden,
				"Message":    "授权失败",
			})
			c.Abort()
			return
		}
		// 解析令牌，parts[1]就是tokenString
		claims, err := common.ParseToken(parts[1])
		// 令牌错误导致的授权失败
		if err != nil {
			c.JSON(http.StatusForbidden, gin.H{
				"StatusCode": http.StatusForbidden,
				"Message":    "授权失败",
			})
			c.Abort()
			return
		}
		// 授权成功
		// 将当前请求中的用户信息保存在请求的上下文中
		c.Set("username", claims.Username)
		log.Println("授权成功", "授权用户[", claims.Username, "]")
		c.Next()
	}
}
