package router

import (
	"gitee.com/dabolau/lakaiService/controller"
	"gitee.com/dabolau/lakaiService/middleware"
	"github.com/gin-gonic/gin"
)

// User 用户路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func User(r *gin.Engine) {
	// 用户分组
	userGroup := r.Group("/user")
	// 使用中间件
	userGroup.Use(middleware.AuthToken())
	{
		// 用户信息
		userGroup.GET("/", controller.UserHandler)
		// 用户详情
		userGroup.GET("/detail", controller.UserDetailHandler)
		// 用户添加
		userGroup.POST("/add", controller.UserAddHandler)
		// 用户编辑
		userGroup.POST("/change", controller.UserChangeHandler)
		// 用户删除
		userGroup.POST("/delete", controller.UserDeleteHandler)
	}
}
