package router

import (
	"gitee.com/dabolau/lakaiService/controller"
	"github.com/gin-gonic/gin"
)

// URL 资源路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func URL(r *gin.Engine) {
	// 资源分组
	urlGroup := r.Group("/url")
	{
		// 资源信息
		urlGroup.GET("/", controller.URLHandler)
		// 资源详情
		urlGroup.GET("/detail", controller.URLDetailHandler)
		// 资源添加
		urlGroup.POST("/add", controller.URLAddHandler)
		// 资源编辑
		urlGroup.POST("/change", controller.URLChangeHandler)
		// 资源删除
		urlGroup.POST("/delete", controller.URLDeleteHandler)
	}
}
