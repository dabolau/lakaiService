package router

import (
	"gitee.com/dabolau/lakaiService/controller"
	"github.com/gin-gonic/gin"
)

// Feedback 反馈路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Feedback(r *gin.Engine) {
	// 反馈分组
	feedbackGroup := r.Group("/feedback")
	{
		// 反馈信息
		feedbackGroup.GET("/", controller.FeedbackHandler)
		// 反馈详情
		feedbackGroup.GET("/detail", controller.FeedbackDetailHandler)
		// 反馈添加
		feedbackGroup.POST("/add", controller.FeedbackAddHandler)
		// 反馈编辑
		feedbackGroup.POST("/change", controller.FeedbackChangeHandler)
		// 反馈删除
		feedbackGroup.POST("/delete", controller.FeedbackDeleteHandler)
	}
}
