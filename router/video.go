package router

import (
	"gitee.com/dabolau/lakaiService/controller"
	"github.com/gin-gonic/gin"
)

// Video 视频路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Video(r *gin.Engine) {
	// 视频分组
	videoGroup := r.Group("/video")
	{
		// 视频信息
		videoGroup.GET("/", controller.VideoHandler)
		// 视频详情
		videoGroup.GET("/detail", controller.VideoDetailHandler)
		// 视频添加
		videoGroup.POST("/add", controller.VideoAddHandler)
		// 视频编辑
		videoGroup.POST("/change", controller.VideoChangeHandler)
		// 视频删除
		videoGroup.POST("/delete", controller.VideoDeleteHandler)
	}
}
