package router

import (
	"gitee.com/dabolau/lakaiService/controller"
	"github.com/gin-gonic/gin"
)

// Account 帐号路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Account(r *gin.Engine) {
	// 帐号分组
	accountGroup := r.Group("/account")
	{
		// 帐号信息
		accountGroup.GET("/", controller.AccountHandler)
		// 帐号登录
		accountGroup.POST("/login", controller.LoginHandler)
		// 帐号注册
		accountGroup.POST("/register", controller.RegisterHandler)
		// 修改密码
		accountGroup.POST("/changepassword", controller.ChangePasswordHandler)
		// 找回密码
		accountGroup.POST("/getpassword", controller.GetPasswordHandler)
		// 帐号注销
		accountGroup.POST("/logout", controller.LogoutHandler)
	}
}
