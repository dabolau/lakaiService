package router

import (
	"gitee.com/dabolau/lakaiService/controller"
	"github.com/gin-gonic/gin"
)

// Home 首页路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Home(r *gin.Engine) {
	// 网页未找到
	r.NoRoute(controller.NotFoundHandler)
	// 定义路由
	r.GET("/", controller.HomeHandler)
}
