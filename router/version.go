package router

import (
	"gitee.com/dabolau/lakaiService/controller"
	"github.com/gin-gonic/gin"
)

// Version 版本路由
// https://gin-gonic.com/docs/examples/http-method/
// https://gin-gonic.com/docs/examples/grouping-routes/
func Version(r *gin.Engine) {
	// 版本分组
	versionGroup := r.Group("/version")
	{
		// 版本信息
		versionGroup.GET("/", controller.VersionHandler)
		// 版本详情
		versionGroup.GET("/detail", controller.VersionDetailHandler)
		// 版本添加
		versionGroup.POST("/add", controller.VersionAddHandler)
		// 版本编辑
		versionGroup.POST("/change", controller.VersionChangeHandler)
		// 版本删除
		versionGroup.POST("/delete", controller.VersionDeleteHandler)
	}
}
