package model

import "gorm.io/gorm"

// Version 版本结构体
// https://gorm.io/zh_CN/docs/models.html
type Version struct {
	gorm.Model         // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	Name        string `form:"name" json:"Name"`
	Version     string `form:"version" json:"Version"`
	Description string `form:"description" json:"Description" gorm:"type:text"`
	URL         string `form:"url" json:"URL"`
}
