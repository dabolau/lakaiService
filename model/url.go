package model

import "gorm.io/gorm"

// URL 资源结构体
// https://gorm.io/zh_CN/docs/models.html
type URL struct {
	gorm.Model        // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	Name       string `form:"name" json:"Name"`
	Video      string `form:"video" json:"Video"`
	Tag        string `form:"tag" json:"Tag"`
	URL        string `form:"url" json:"URL"`
}
