package model

import "gorm.io/gorm"

// Video 视频结构体
// https://gorm.io/zh_CN/docs/models.html
type Video struct {
	gorm.Model         // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	Name        string `form:"name" json:"Name"`
	Category    string `form:"category" json:"Category"`
	Type        string `form:"type" json:"Type"`
	Area        string `form:"area" json:"Area"`
	Year        string `form:"year" json:"Year"`
	Actor       string `form:"acotr" json:"Actor"`
	Director    string `form:"director" json:"Director"`
	Status      string `form:"status" json:"Status"`
	Description string `form:"description" json:"Description" gorm:"type:text"`
	URL         string `form:"url" json:"URL"`
}
