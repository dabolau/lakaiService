package model

import "gorm.io/gorm"

// User 用户结构体
// https://gorm.io/zh_CN/docs/models.html
type User struct {
	gorm.Model         // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	Username    string `form:"username" json:"Username"`
	Email       string `form:"email" json:"Email"`
	Phone       string `form:"phone" json:"Phone"`
	Password    string `form:"password" json:"Password"`
	Nickname    string `form:"nickname" json:"Nickname"`
	Description string `form:"description" json:"Description" gorm:"type:text"`
}
