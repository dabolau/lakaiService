package model

import "gorm.io/gorm"

// Feedback 反馈结构体
// https://gorm.io/zh_CN/docs/models.html
type Feedback struct {
	gorm.Model         // 包含了ID, CreatedAt, UpdatedAt, DeletedAt
	Name        string `form:"name" json:"Name"`
	Description string `form:"description" json:"Description" gorm:"type:text"`
	Email       string `form:"email" json:"Email"`
}
