package database

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// var (
// 	// DB 数据库
// 	DB *gorm.DB
// )

// OpenMySQL 数据库连接
// https://gorm.io/zh_CN/docs/connecting_to_the_database.html
func OpenMySQL() (err error) {
	// 数据来源名称
	// 获取详情 https://github.com/go-sql-driver/mysql#dsn-data-source-name
	dsn := "root:@tcp(localhost:3306)/lakai?charset=utf8mb4&parseTime=True&loc=Local"
	// 数据库连接
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}
	return nil
}
