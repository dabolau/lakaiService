package database

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// var (
// 	// DB 数据库
// 	DB *gorm.DB
// )

// OpenSQLite3 数据库连接
// https://gorm.io/zh_CN/docs/connecting_to_the_database.html
func OpenSQLite3() (err error) {
	// 数据来源名称
	dsn := "./db.sqlite3"
	// 数据库连接
	DB, err = gorm.Open(sqlite.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}
	return nil
}
