package common

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

// JwtClaims 结构体
type JwtClaims struct {
	Username           string `json:"username"`
	jwt.StandardClaims        // jwt包中的默认字段
}

var (
	// 过期时间
	_jwtExpireTime = time.Second * (30 * 86400) // 30*86400=30天
	// 签发人
	_jwtIssuer = "dablau"
	// 密钥
	_jwtSecret = []byte("春花秋月何时了")
)

// GenerateToken 生成令牌
// https://godoc.org/github.com/dgrijalva/jwt-go#NewWithClaims
func GenerateToken(username string) (string, error) {
	// 自定义声明
	jwtClaims := JwtClaims{
		Username: username, // 自定义字段
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(_jwtExpireTime).Unix(), // 过期时间
			Issuer:    _jwtIssuer,                            // 签发人
		},
	}
	// 使用指定的签名方法创建签名对象
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaims)
	// 使用指定的密钥（_jwtSecret）签名并获得完整的编码后的字符串token
	tokenString, err := token.SignedString(_jwtSecret)
	return tokenString, err
}

// ParseToken 解析令牌
// https://godoc.org/github.com/dgrijalva/jwt-go#ParseWithClaims
func ParseToken(tokenString string) (*JwtClaims, error) {
	// 解析令牌
	token, err := jwt.ParseWithClaims(tokenString, &JwtClaims{}, func(token *jwt.Token) (interface{}, error) {
		return _jwtSecret, nil
	})
	// 解析声明
	jwtClaims, ok := token.Claims.(*JwtClaims)
	// 校验令牌
	if ok && token.Valid {
		return jwtClaims, nil
	}
	return nil, err
}
