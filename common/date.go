package common

import (
	"time"
)

// GetYear 获取年，输出格式“2006”
func GetYear() (date string) {
	date = time.Now().Format("2006")
	return
}

// GetMonth 获取月，输出格式“01”
func GetMonth() (date string) {
	date = time.Now().Format("01")
	return
}

// GetDay 获取日，输出格式“02”
func GetDay() (date string) {
	date = time.Now().Format("02")
	return
}

// GetHour 获取时，输出格式“15”
func GetHour() (date string) {
	date = time.Now().Format("15")
	return
}

// GetMinute 获取分，输出格式“04”
func GetMinute() (date string) {
	date = time.Now().Format("04")
	return
}

// GetSecond 获取秒，输出格式“05”
func GetSecond() (date string) {
	date = time.Now().Format("05")
	return
}

// GetYearMonthDay 获取年月日，输出格式“2006-01-02”
func GetYearMonthDay() (date string) {
	date = time.Now().Format("2006-01-02")
	return
}

// GetHourMinuteSecond 获取时分秒，输出格式“15:04:05”
func GetHourMinuteSecond() (date string) {
	date = time.Now().Format("15:04:05")
	return
}

// GetYearMonthDayHourMinuteSecond 获取年月日时分秒，输出格式“2006-01-02 15:04:05”
func GetYearMonthDayHourMinuteSecond() (date string) {
	date = time.Now().Format("2006-01-02 15:04:05")
	return
}
