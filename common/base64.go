package common

import (
	"encoding/base64"
)

var (
	// _str = "ZHONGGUOnihao123"
	_str = "中国你好123"
)

// EncodeBase64 base64加密字符串
func EncodeBase64(str string) (ecs string) {
	strbytes := []byte(str)
	ecs = base64.StdEncoding.EncodeToString(strbytes)
	return ecs
}

// DecodeBase64 base64解密字符串
func DecodeBase64(str string) (dcs string) {
	strbytes, _ := base64.StdEncoding.DecodeString(str)
	dcs = string(strbytes)
	return dcs
}
